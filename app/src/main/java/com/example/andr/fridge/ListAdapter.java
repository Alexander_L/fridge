package com.example.andr.fridge;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class ListAdapter extends BaseAdapter {
    Context ctx;
    protected ArrayList<Product> holdProducts; //= ProductsInFridge.getInstance().getHoldProducts();
    LayoutInflater layoutInflater;

    ListAdapter(Context context, ArrayList<Product> products) {
        ctx = context;
        holdProducts = products;
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override public int getCount() {
        return holdProducts.size();
    }

    @Override public Object getItem(int position) {
        return holdProducts.get(position);
    }

    @Override public long getItemId (int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.two_columns_list, parent, false);
        }

        Product p = getProduct(position);
        ((TextView) view.findViewById(R.id.TextFirst)).setText(p.getFoodToHold().name());
        ((TextView) view.findViewById(R.id.DightSecond)).setText((String.valueOf(p.getKeepDays())));

        return view;
    }

    Product getProduct (int position) {
        return ((Product) getItem(position));
    }


}
