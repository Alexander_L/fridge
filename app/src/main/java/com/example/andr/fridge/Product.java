package com.example.andr.fridge;

class Product {

    //protected FoodStorage.Food product = FoodStorage.Food.MILK; //FoodStorage.getInstance().testGetFood();
    private FoodStorage.Food foodToHold;
    private int keepDays;//DateWorker.getInstance().testKeepDays(); Валится с ошибкой вывода списка, если не String


    Product (FoodStorage.Food foodToHold, int keepDays){
        this.foodToHold = foodToHold;
        this.keepDays = keepDays;
    }

    protected FoodStorage.Food getFoodToHold() {
        return foodToHold;
    }

    protected int getKeepDays() {
        return keepDays;
    }

   /* protected int getKeepDays () { //перенести преобразование в адаптер
        setKeepDays();
        return keepDays;
    }

    protected FoodStorage.Food getFoodToHold() {
        setFoodToHold();
        return foodToHold;
    }

    private void setKeepDays () {
        keepDays = DateWorker.getInstance().testKeepDays();
    }

    private void setFoodToHold () {
        foodToHold = FoodStorage.getInstance().testGetFood();
    }
*/
}
