package com.example.andr.fridge;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class FoodNameInput extends AppCompatActivity {

    protected static FoodNameInput instance;
    private AutoCompleteTextView autoCompleteTextView;
    private String foodName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_name_input);
        instance = this;

        autoCompleteTextView = findViewById(R.id.foodNameInputAutoCompleteTextView);
        autoCompleteTextView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, FoodStorage.getInstance().getFoodStrings()));


    }

    public void onClickBtnAddFoodName(View view) {
        if (!autoCompleteTextView.getText().toString().isEmpty()) { //проверить корректность
            setFoodName(autoCompleteTextView.getText().toString());
            DialogFragment newFragment = new DatePickerFragment(); //move to FoodNameInput
            newFragment.show(getSupportFragmentManager(), "datePicker");
        }
    }

    protected static FoodNameInput getInstance() {
        return instance;
    }

    protected String getFoodName() {
        return foodName;
    }

    protected void setFoodName(String inputedfoodName) {
        foodName = inputedfoodName;
    }



}
