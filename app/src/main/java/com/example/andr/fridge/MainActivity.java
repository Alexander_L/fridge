package com.example.andr.fridge;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    protected static MainActivity instance;

    protected ListView lvHoldProductsList;
    protected ArrayList <Product> holdProducts;
    ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;


        lvHoldProductsList = findViewById(R.id.productsHoldListView);
        holdProducts = ProductsInFridge.getInstance().getHoldProducts();

        listAdapter = new ListAdapter(this, holdProducts);

        lvHoldProductsList.setAdapter(listAdapter);

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButtonAdd);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFoodNameInputIntent();
                /*DialogFragment newFragment = new DatePickerFragment(); //move to FoodNameInput
                newFragment.show(getSupportFragmentManager(), "datePicker");*/

                listAdapter.notifyDataSetChanged();
            }
        });

    }

    protected Product setProductInFridge(FoodStorage.Food foodToHold, int keepDays) {
        return new Product(foodToHold, keepDays);
    }

    protected static MainActivity getInstance () {
        return instance;
    }

    private void getFoodNameInputIntent() {
        Intent intent = new Intent(this, FoodNameInput.class);
        startActivity(intent);
    }

    /*protected void addProductInFridge () {
        holdProducts.add(setProductInFridge());
    }*/




}
