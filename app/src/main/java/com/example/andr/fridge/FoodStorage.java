package com.example.andr.fridge;

import java.util.Arrays;

class FoodStorage {

    protected static FoodStorage instance;

    protected enum Food {MILK, BREAD, CHEESE, POTATO, APPLE, MEAT, EGGS, ONION};

    protected static FoodStorage getInstance() {
        if (instance == null) {
            instance = new FoodStorage();
        }
        return instance;
    }

    protected String[] getFoodStrings() {
        return Arrays.toString(Food.values()).replaceAll("^.|.$", "").split(", ");
    }

    protected Food testGetFood() {
        return Food.MILK;
    }
}
